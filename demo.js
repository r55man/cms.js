
/*
const Console = cmsjs.Console.create('Main','holy-grail');
Console.setTitle('My Page');

cmsjs.Component.create('header', (elt) => {
	const h1 = document.createElement('h1');
	h1.innerHTML = 'My Heading';
	elt.appendChild(h1);
});

Console.getPanel('Header').addComponent('header');
*/

cmsjs.Logger.overrideAll(true);

const log = cmsjs.Logger.create('cmsjs-demo');

//cmsjs.Logger.LogLevel.list().forEach((item) => {
//	log[item]('testing log level:',item);
//});

log.info('setting up console');

const Console = {};
const Panel = {}; // rem: rowStart, colStart, rowSpan, colSpan

/**
 * Main Console
 */
Console.Main = cmsjs.Console.create('Main','grid-basic');
Console.Main.setTitle('CMS.js Demo');
Console.Main.setFaviconURL('favicon.ico');
Console.Main.setGridRows('auto 1fr auto auto');
Console.Main.setGridColumns('auto 1fr auto');
Panel.Main = {};
Panel.Main.Header = Console.Main.addPanel('Header','1,1,1,3');
Panel.Main.Left = Console.Main.addPanel('Left','2,1,1,1');
Panel.Main.Content = Console.Main.addPanel('Content','2,2,1,1');
Panel.Main.Right = Console.Main.addPanel('Right','2,3,1,1');
Panel.Main.Footer = Console.Main.addPanel('Footer','3,1,1,3');
Panel.Main.Editor = Console.Main.addPanel('Editor','4,1,1,3');
Panel.Main.Header.element.style.borderBottom = 'solid 1px';
Panel.Main.Left.element.style.borderRight = 'solid 1px';
Panel.Main.Right.element.style.borderLeft = 'solid 1px';
Panel.Main.Footer.element.style.borderTop = 'solid 1px';
Panel.Main.Editor.element.style.borderTop = 'solid 1px';
Console.Main.addStyle('ul { color: red; }');

/**
 * Mobile Console
 */
Console.Mobile = cmsjs.Console.create('Mobile','grid-basic');
Console.Mobile.setTitle('CMS.js Demo');
Console.Mobile.setFaviconURL('favicon.ico');
Console.Mobile.setGridRows('auto 1fr auto');
Console.Mobile.setGridColumns('1fr');
Panel.Mobile = {};
Panel.Mobile.Header = Console.Mobile.addPanel('Header','1,1,1,1');
Panel.Mobile.Content = Console.Mobile.addPanel('Content','2,1,1,1');
Panel.Mobile.Footer = Console.Mobile.addPanel('Footer','3,1,1,1');
Panel.Mobile.Header.element.style.minHeight = '100px';
Panel.Mobile.Header.element.style.borderBottom = 'solid 1px';
Panel.Mobile.Footer.element.style.borderTop = 'solid 1px';
Panel.Mobile.Footer.element.style.minHeight = '100px';
Console.Mobile.addStyle('ul { background-color: red; }');

/**
 * Max Console
 */
Console.Max = cmsjs.Console.create('Max','grid-basic');
Console.Max.setTitle('CMS.js Demo');
Console.Max.setFaviconURL('favicon.ico');
Console.Max.setGridRows('1fr');
Console.Max.setGridColumns('auto auto 1fr auto auto');
Panel.Max = {};
Panel.Max.Header = Console.Max.addPanel('Header','1,1,1,1');
Panel.Max.Content = Console.Max.addPanel('Content','1,2,1,1');
Panel.Max.Footer = Console.Max.addPanel('Footer','1,3,1,1');
Panel.Max.Left = Console.Max.addPanel('Left','1,4,1,1');
Panel.Max.Right = Console.Max.addPanel('Right','1,5,1,1');
Panel.Max.Header.element.style.minHeight = '100px';
Panel.Max.Header.element.style.borderBottom = 'solid 1px';
Panel.Max.Footer.element.style.backgroundColor = '#fef';
Panel.Max.Header.element.style.backgroundColor = '#fef';
Panel.Max.Footer.element.style.borderTop = 'solid 1px';
Panel.Max.Footer.element.style.minHeight = '100px';
Panel.Max.Footer.element.style.minWidth = '100px';


/**
 *
 */
const menuLink = document.createElement('a');
menuLink.style.display = 'block';
menuLink.style.padding = '0.5rem';
menuLink.style.borderLeft = 'solid 1px black';
menuLink.style.borderBottom = 'solid 1px black';
menuLink.style.borderRadius = '0px 0px 0px 5px';
menuLink.style.position = 'absolute';
menuLink.style.zIndex = '10';
menuLink.style.top = '0px';
menuLink.style.right = '0px';
menuLink.innerHTML = '\u2630';
menuLink.addEventListener('click', () => {
	const panelElt = Panel.Main.Left.element;
	//panelElt.style.width = 'auto';
	panelElt.style.transition = 'width 1.0s ease';
	if ( panelElt.style.display === 'none' ) {
		panelElt.style.transition = 'width 1.0s ease';
		panelElt.style.display = 'block';
		setTimeout(() => { panelElt.style.width = '150px'; }, 10);
		//panelElt.style.width = 'auto';
		//panelElt.style.width = '150px';
	}
	else {
		panelElt.style.width = '0px';
		setTimeout(() => { panelElt.style.width = '0px'; }, 10);
		setTimeout(() => { panelElt.style.display = 'none'; }, 500);
	}
});



cmsjs.Component.create('header', (component) => {
	const wrapper = component.wrapper;
	wrapper.style.backgroundColor = '#eee';
	wrapper.style.padding = '0.25rem 0.5rem';
	const elt = component.element;
	const h1 = document.createElement('h1');
	h1.innerHTML = 'CMS.js';
	h1.style.fontSize = '1.75rem';
	h1.style.margin = '0px';
	elt.appendChild(h1);
});

cmsjs.Component.create('content', (component) => {
	component.setScrollable(true);
	const wrapper = component.wrapper;
	wrapper.style.padding = '0.25rem 0.5rem';
	const elt = component.element;
	elt.innerHTML = cmsjs.Component.Mockup.text(100,4);
});

cmsjs.Component.create('menu', (component) => {
	component.setScrollable(true);
	const wrapper = component.wrapper;
	wrapper.style.padding = '0.25rem 0.5rem';
	const elt = component.element;
	elt.innerHTML = cmsjs.Component.Mockup.list(50);
	//elt.style.padding = '1rem';
	//const config = {};
	//cmsjs.Controls.create('MainMenu','menu',config);
});

cmsjs.Component.create('menu2', (component) => {
	component.setScrollable(true);
	const wrapper = component.wrapper;
	wrapper.style.padding = '0.25rem 0.5rem';
	const elt = component.element;
	//elt.style.padding = '0.25rem 0.5rem';
	elt.innerHTML = cmsjs.Component.Mockup.divs(10,100,100);
	//elt.style.padding = '1rem';
	//const config = {};
	//cmsjs.Controls.create('MainMenu','menu',config);
});

cmsjs.Component.create('footer', (component) => {
	const wrapper = component.wrapper;
	wrapper.style.backgroundColor = '#eee';
	wrapper.style.padding = '0.25rem 0.5rem';
	const elt = component.element;
	elt.style.textAlign = 'center';
	const link = document.createElement('a');
	link.href = '#';
	link.addEventListener('click', (event) => {
		event.preventDefault();
		cmsjs.Console.restoreOriginalDocument();
	});
	link.textContent = 'Return to Loading Page';
	elt.appendChild(link);
});



/**
 * Attach components
 */
Panel.Main.Header.addComponent('header');
Panel.Main.Content.addComponent('content');
Panel.Main.Footer.addComponent('footer');
Panel.Main.Left.addComponent('menu');
Panel.Main.Right.addComponent('menu2');

Panel.Mobile.Header.addComponent('header');
Panel.Mobile.Content.addComponent('content');
Panel.Mobile.Footer.addComponent('footer');

Panel.Max.Header.addComponent('header');
Panel.Max.Content.addComponent('content');
Panel.Max.Footer.addComponent('footer');
Panel.Max.Left.addComponent('menu');
Panel.Max.Right.addComponent('menu2');

let demo_is_initted = false;
function initDemo() {
	if ( ! demo_is_initted) {
		window.addEventListener('resize', () => {
			if ( window.innerWidth < 768 ) {
				cmsjs.Console.setActiveConsole('Mobile');
			}
			else if ( window.innerWidth < 1280 ) {
				cmsjs.Console.setActiveConsole('Main');
			}
			else {
				cmsjs.Console.setActiveConsole('Max');
			}
		});
		demo_is_initted = true;
	}
	window.dispatchEvent(new Event('resize'));
}

//cmsjs.Console.setActiveConsole('Main');

