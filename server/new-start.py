'''
A simple HTTP server for use with cms.js. 

Do not change the location of this file. It must reside in the 'server'
subdirectory of the cms.js installation in order to function properly.

Run this script using 'python start.sh' to start the server.

By default, it uses port 8000. This can be configured via the options.

This does not do any module <-> filesystem path resolution. That all
takes place on the client. Path requests must map to an actual file 
on the filesystem.

'''
import http.server
import socketserver
import argparse
import logging
import json

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('http_server')

class Handler(http.server.BaseHTTPRequestHandler):
    def log_request(self, code='-', size='-'):
        # Override log_request to include bytes sent
        logger.info('%s - - [%s] "%s" %s %s',
                     self.address_string(),
                     self.log_date_time_string(),
                     self.requestline,
                     str(code),
                     str(size))

    def do_GET(self):
        # get absolute path to script
        server_script_path = os.path.abspath(sys.argv[0])
        server_subdir = os.path.dirname(server_script_path)
        cms_root = os.path.abspath(os.path.join(os.path.dirname(server_script_path), '..'))
        hostname = self.headers.get('Host')
        file_path = os.path.join(cms_root, self.path.lstrip('/'))
        if os.path.exists(file_path) and os.path.isfile(file_path):
            mime_type, _ = mimetypes.guess_type(file_path)
            if mime_type:
                with open(file_path, 'rb') as file:
                    self.send_response(200)
                    self.send_header('Content-type', mime_type)
                    self.end_headers()
                    self.wfile.write(file.read())
            else:
                self.send_error(415, 'Unsupported media type')
        else:
            self.send_error(404, 'File not found')
￼
￼
￼
￼

        response_data = {'message': 'GET request received'}
        self.send_json_response(200, response_data)
          # Assuming requested path is the file path relative to the current directory

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode('utf-8')
        response_data = {'message': 'POST request received', 'data_received': post_data}
        self.send_json_response(200, response_data)


    def do_POST(self):
        content_type = self.headers.get('Content-Type', '')

        if self.path != '/' or content_type == '':
            self.send_error(400, 'Bad request')
            return

        if content_type.startswith('multipart/form-data'):
            boundary = content_type.split('boundary=')[1].encode('utf-8')
            self.receive_files(boundary)
        else:
            self.send_error(400, 'Unsupported content type')

    def do_POST(self):
        content_type = self.headers.get('Content-Type', '')
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        
        if content_type.startswith('application/json'):
            # If content-type is JSON, decode JSON data
            try:
                data = json.loads(post_data.decode('utf-8'))
                response_data = {'message': 'POST request received', 'data': data}
                self.send_json_response(200, response_data)
            except json.JSONDecodeError as e:
                self.send_error(400, 'Invalid JSON data', str(e))
        else:
            # Otherwise, treat data as binary (e.g., image)
            response_data = {'message': 'Binary data received', 'data_length': len(post_data)}
            self.send_json_response(200, response_data)

       def do_POST(self):
        content_type = self.headers.get('Content-Type', '')

        if content_type.startswith('multipart/form-data'):
            # If content-type is multipart/form-data, it indicates multiple files
            boundary = content_type.split('boundary=')[1].encode('utf-8')
            self.receive_files(boundary)
        else:
            self.send_error(400, 'Unsupported content type')

    def receive_files(self, boundary):
        # Read the POST data until the boundary
        data = self.rfile.readline()
        while data and not data.startswith(boundary):
            data = self.rfile.readline()

        # Process each part (file) in the multipart request
        while data:
            headers = {}
            # Read headers for the part
            while True:
                line = self.rfile.readline().decode('utf-8').strip()
                if line == '':
                    break
                key, value = line.split(':', 1)
                headers[key.strip()] = value.strip()

            # Read content (file data) for the part
            content_length = int(headers.get('Content-Length', 0))
            file_data = self.rfile.read(content_length)

            # Process the file data as needed
            # For example, save the file to disk
            filename = headers.get('Content-Disposition', '').split('filename=')[1].strip('"')
            with open(filename, 'wb') as f:
                f.write(file_data)

            # Read the next part
            data = self.rfile.readline()
            while data and not data.startswith(boundary):
                data = self.rfile.readline()

        # Send a response indicating successful upload
        response_data = {'message': 'Files uploaded successfully'}
        self.send_json_response(200, response_data)


    def receive_files(self, boundary):
        # Implementation for handling multipart form data
        pass

    def send_json_response(self, status_code, data):
        self.send_response(status_code)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        response_content = json.dumps(data).encode('utf-8')
        self.wfile.write(response_content)



def run(server_class=HTTPServer, handler_class=CustomRequestHandler, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting server on port {port}...')
    httpd.serve_forever()

def get_arguments():
    """
    Parse command-line arguments.
    """
    parser = argparse.ArgumentParser(description="Start an HTTP server.")
    parser.add_argument("--port", type=int, default=8000, help="Port number to start the server (default: 8000)")
    return parser.parse_args()

# Get the command-line arguments
args = get_arguments()

# Set up the server with the specified port and custom request handler
with socketserver.TCPServer(("", args.port), Handler) as httpd:
    print("Server started at port", args.port)
    # Start the server
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        # Handle Ctrl+C gracefully
        print("\nStopping server...")
        httpd.shutdown()
        print("Server stopped.")


if __name__ == '__main__':
    run()

