'''
A simple HTTP server for use with cms.js. 

Do not change the location of this file. It must reside in the 'server'
subdirectory of the cms.js installation in order to function properly.

Run this script using 'python start.sh' to start the server.

By default, it uses port 8000. This can be configured via the options.

This does not do any module <-> filesystem path resolution. That all
takes place on the client. Path requests must map to an actual file 
on the filesystem.

'''
from http.server import SimpleHTTPRequestHandler, HTTPServer
import os
import sys

server_script_path = os.path.abspath(sys.argv[0])
print(server_script_path)
server_subdir = os.path.dirname(server_script_path)
print(server_subdir)
cms_root = os.path.abspath(os.path.join(os.path.dirname(server_script_path), '..'))
print(cms_root)
os.chdir(cms_root)

class Handler(SimpleHTTPRequestHandler):
    def send_header(self, keyword, value):
        if keyword.lower() == 'cache-control':
            value = 'no-cache'
        super().send_header(keyword, value)
    '''
    def translate_path(self, path):
        root = cms_root
        path = super().translate_path(path)
        print(path)
        print(root)
        output = os.path.join(root, path.lstrip('/'))
        print(output)
        return output
    '''

def run(server_class=HTTPServer, handler_class=Handler, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting HTTP server on port {port}...')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print('Stopping HTTP server')

if __name__ == '__main__':
    run()
