// init
let Module, self, ui, $;
export const actions = new Map();
export async function load(MODULE,SELF) {
	Module = MODULE;
	self = SELF;
	ui = await Module.load('/UI/');
	$ = ui.$;
}
export async function init() {
}


/*
 * Root action
 */
actions.set('.', async (params) => {
	const body = document.body;
	body.removeAttribute('style');
	body.innerHTML = '';
	let p;
	
	p = $.ce('p');
	p.innerHTML = 'Welcome to the root module :)';
	body.appendChild(p);
	
	p = $.ce('p');
	p.innerHTML = '<a href="' + self.getVirtualURL('redirect') + '">Test a redirect</a>';
	body.appendChild(p);

	p = $.ce('p');
	p.innerHTML = '<a href="' + self.getVirtualURL('/UI/') + '">Go to /UI/ module</a>';
	body.appendChild(p);

	p = $.ce('p');
	p.innerHTML = '<a href="' + self.getVirtualURL('pftk/') + '">Go to pftk/ module</a>';
	body.appendChild(p);

	
	//const ui = await self.loadModule('/UI/');
	//console.error('ui',ui);
	//params.set('module',self);
	//ui.script.drawPage(params);
});

/*
 * Default action
 */
actions.set('*', async (params) => {
	const url = self.path + self.env('Action');
	if ( self.env('Action') === '*' ) {
		$.ss('body').innerHTML = '<p>This is not meant to be called directly... :/</p>';
	}
	else {
		$.ss('body').innerHTML = '<p>Unfortunately, "' + url + '" was not found :(</p>';
	}
});

actions.set('redirect', async (params) => {
	self.redirect('UI');
});

actions.set('blob', async (params) => {
	const blob = await self.fetchFile('test.avif');
	self.debug('blob=',blob);
	const blobURL = URL.createObjectURL(blob);
	self.debug('blobURL=',blobURL);
});

actions.set('UI', async (params) => {
	$.ss('body').innerHTML = '<p><a href="' + self.getVirtualURL('/UI/') + '">Click me</a></p>';
});

