

export const self = new cmsjs.Module();

/*
 * Load function
 */
self.load = function() => {
};

/*
 * Initialization function
 */
self.init = function() => {
};


/*
 * Root action
 */
self.actions.set('.', async (params) => {
	self.log.trace('Redirecting to "overview".',params);
	self.redirect('overview');
	self.action('overview',params);
});

/*
 * Default action
 */
self.actions.set('*', async (params) => {
	self.log.trace('There is no default action for this module.',params);
	const div = $.ce('div');
	const h2 = $.ce('h2');
	h2.innerHTML = '404 Not Found';
	div.append(h2);
	const p1 = $.ce('p');
	p1.innerHTML = 'The page you are looking for cannot be found. :(';
	div.append(p1);
	await ui.drawPage(self,div);
});


/*
 * Content: Overview
 */
actions.set('overview', async (params) => {
	const div = $.ce('div');
	const h2 = $.ce('h2');
	h2.innerHTML = 'Overview';
	div.append(h2);
	const p1 = $.ce('p');
	p1.innerHTML = 'Valid actions for the ' + self.path + ' module:';
	div.append(p1);
	const actionUL = $.ce('ul');
	div.append(actionUL);
	Array.from(actions.keys()).sort().forEach((item) => {
		const li = $.ce('li');
		li.innerHTML = item;
		actionUL.append(li);
	});
	const p2 = $.ce('p');
	p2.innerHTML = '<a href="' + self.getVirtualURL('layouts') + '">View Layouts</a>';
	self.log.debug('p2',p2);
	div.append(p2);
	await ui.drawPage(self,div);
});

/*
 * Content: Layouts
 */
actions.set('layouts', async (params) => {
	if ( params.get('layout') ) {
	}
	const div = $.ce('div');
	const h2 = $.ce('h2');
	h2.innerHTML = 'Layouts';
	div.append(h2);
	const p1 = $.ce('p');
	p1.innerHTML = 'Select a layout:';
	p1.style.fontWeight = 'bold';
	p1.style.fontSize = '16pt';
	div.append(p1);
	const layoutUL = $.ce('ul');
	div.append(layoutUL);
	allLayouts.forEach((value,key) => {
		const a = $.ce('a');
		a.href = self.getVirtualURL('draw?layout=' + key + '&view=demo');
		a.innerHTML = value.get('label');
		const li = $.ce('li');
		li.append(a);
		li.style.padding = '0.25em';
		layoutUL.append(li);
	});
	await ui.drawPage(self,div);
});

// Takes a module and a map and builds a menu.
export function buildMenuFromMap(module,map,lang) {
	const wrapper = $.ce('ol');
	wrapper.style.listStyleType = 'none';
	wrapper.style.margin = '0';
	wrapper.style.padding = '0';
	map.forEach((item,index) => {
		const li = $.ce('li');
		li.style.listStyleType = 'none';
		li.style.margin = '0';
		li.style.padding = '0';
		wrapper.append(li);
		const text = item['text'];
		let action = item['action'] || null;
		if ( action && action !== null ) {
			if ( action === '.' ) { action = module.getVirtualURL(); }
			else if ( action.endsWith('/.') ) { action = module.getVirtualURL(action.slice(0,-1)); }
			else if ( action === '..' ) { action = module.getParent().getVirtualURL(); }
			else { action = module.getVirtualURL(action); }
			const a = document.createElement('a');
			a.href = action;
			a.textContent = item['text'];
			if ( lang && item['lang'] && item['lang'][lang] ) {
				a.textContent = item['lang'][lang];
			}
			if ( item['class'] ) {
				li.classList.add(item['class']);
			}
			//console.log(self.env('Module') + self.env('Action') +  ' , ' + action.slice(1));
			if ( self.env('Module') + self.env('Action') === action.slice(1) ) {
				a.classList.add('active');
			}
			else {
				a.classList.remove('active');
			}
			li.append(a);
			const submenu = item['submenu'];
			if ( submenu ) {
				const submenuDiv = buildMenuFromMap(module,submenu,lang);
				submenuDiv.classList.add('cms-ui-submenu');
				submenuDiv.style.overflow = 'hidden';
				submenuDiv.style.transition = 'max-height 1s, opacity 1s ease';
				if ( item['submenu.expand'] ) {
					submenuDiv.style.maxHeight = '1000px';
					submenuDiv.style.opacity = '1';
				}
				else {
					submenuDiv.style.maxHeight = '0px';
					submenuDiv.style.opacity = '0';
				}
				a.addEventListener('click', (event) => {
					//					event.preventDefault();
					if ( submenuDiv.classList.contains('active') ) {
						submenuDiv.classList.remove('active');
						submenuDiv.style.opacity = '0';
						submenuDiv.style.maxHeight = '0px';
					}
					else {
						const height = submenuDiv.scrollHeight;
						submenuDiv.classList.add('active');
						submenuDiv.style.opacity = '1';
						submenuDiv.style.maxHeight = height + 'px';
					}
				});
				li.append(submenuDiv);
			}
		}
	});
	return wrapper;
}


/**
 * Scrolls to a specific point on the page based on ENV.Hash
 */
export function scrollToHash(elt,adjustment=0) {
	if ( self.env('Hash') ) {
		const targetElement = document.getElementById(self.env('Hash'));
		targetElement.scrollIntoView({
			behavior: 'auto', // auto, instant, smooth
			block: 'start', // start, center, end, nearest
			inline: 'nearest' // start, center, end, nearest
		});
		document.getElementById(elt).scrollBy(0,adjustment);
	}
}

/**
 * Language selector. Call from a module.
 * 
 * Pass an element to bind to, as well as an array of languages.
 */
export function getLanguageSelect(module,id,languages,defaultLanguage) {
	const langMap = {
		bn: '\u09ac\u09be\u0982\u09b2\u09be',
		de: 'Deutsch',
		en: 'English',
		es: 'Espa\u00f1ol',
		fr: 'Fran\u00e7ais',
		hi: '\u0939\u093f\u0928\u094d\u0926\u0940',
		it: 'Italiano',
		ja: '\u65e5\u672c\u8a9e',
		ko: '\ud55c\uad6d\uc5b4',
		pa: '\u062a\u0648\u0646\u0649',
		pt: 'Portugu\u00eas',
		ru: '\u0420\u0443\u0441\u0441\u043a\u0438\u0439',
		tr: 'T\u00fcrk\u00e7e',
		vi: 'Ti\u1ebfng Vi\u1ec7t',
		zh: '\u4e2d\u6587'
		
	};
	const langOptions = [];
	languages.forEach((code) => {
		langOptions.push({ value: code, label: langMap[code] });
	});
	const div = document.createElement('div');
	div.id = id;
	const label = document.createElement('label');
	label.htmlFor = 'lang-select';
	//label.textContent = '\u1F310';
	label.textContent = '🌐';
	label.fontSize = '1.1rem';
	label.style.marginRight = '0.5rem';
	div.appendChild(label);
	const select = document.createElement('select');
	select.style.fontSize = '0.9rem';
	select.style.borderRadius = '0.25rem';
	select.style.padding = '0.1rem';
	select.id = 'lang-select';
	select.name = 'lang';
	langOptions.forEach((option) => {
		const optionElement = document.createElement('option');
		optionElement.value = option.value;
		optionElement.textContent = option.label;
		select.appendChild(optionElement);
	});
	select.addEventListener('change', (event) => {
		const selectedValue = event.target.value;
		module.setCookie('language',selectedValue);
		module.UI.setLanguage(selectedValue);
	});
	if ( module.getCookie('language') ) {
		select.value = module.getCookie('language');
	}
	div.appendChild(select);
	return div;
}

// Required by UI module
export class UI {


	static getTitle() {
		return "cms.js :: mod.UI";
	}

	static getMetaTags() {
		return [
			{ "charset" : "UTF-8" },
			{
				"name" : "viewport",
				"content" : "width=device-width, initial-scale=1.0"
			},
			{
				"name" : "description",
				"content" : "Data Viewer, Save File Analyzer, Save File Editor, and Game Planner for the Owlcat Pathfinder Games: Kingmaker and Wrath of the Righteous (WotR)"
			}
		];
	}

	// Get the style
	static getStyle() {
		return `

a {
	text-decoration: none;
}
.cms-layout-panel {
	border: solid 1px black;
	min-width: 12em;
	min-height: 5vh;
}

/*
 * LANGUAGE SELECTOR
 */
#cms-component-lang-select {
	float: right;
	text-align: center;
	font-weight: bold;
	padding: 0.5em 2em;
}
#cms-component-lang-select label {
	display: none;
	margin-left: 0.25em;
}
#cms-component-lang-select select {
	/* Reset default styles */
	appearance: none;
	-webkit-appearance: none;
	-moz-appearance: none;
	text-indent: 1px;
	text-overflow: '';
	/* Custom styles */
	padding: 4px 8px;
	font-size: 14px;
	border: 1px solid #ccc;
	border-radius: 5px;
	outline: none; /* Remove default focus outline */
	cursor: pointer;
}
/* Change border color on hover and focus */
#cms-component-lang-select select:hover,
#cms-component-lang-select select:focus {
	border-color: #007bff; 
}


/*
 * HEADER
 */
#cms-component-header {
/*	background-color: var(--bgcolor); */
	height: 100px;
	font-family: 'Roboto', serif;
	margin: 0px;
	padding: 0px;
/*	border: solid 1px #333; */
	text-align: center;
/*	width: 100%; */
}
#cms-component-header h1 {
	font-size: 36px;
	padding: 5px 10px;
}
#cms-component-header h1 a {
	color: inherit;
}
#cms-component-header img {
	display: block;
	margin: 5px 10px;
	padding: 0px;
}
#cms-component-header img.logo-kingmaker {
	float: right;
	clear: right;
}
#cms-component-header img.logo-wotr {
	float: left;
	clear: left;
}


/*
 * MENU
 */
#cms-component-menu {
	--menu-bgcolor: #dfd;
	--menu-header-bgcolor: #fdd;
	--menu-spacer-bgcolor: #fdd;
	--menu-active-bgcolor: #ddf;
	--menu-hover-bgcolor: #ddf;
}
#cms-component-menu {
    font-family: 'Almendra', serif;
	text-align: center;
	outline: solid #777 1px;
}
#cms-component-menu a {
	text-decoration: none;
	font-size: 20px;
	font-weight: bold;
	display: block;
	margin: 0px;
	padding: 0px;
	border-top: solid #777 1px;
	background-color: var(--menu-bgcolor);
	padding: 0.6em 0.5em;
}
#cms-component-menu a.header {
	background-color: var(--menu-header-bgcolor) !important;
}
#cms-component-menu a.spacer {
    height: 20px;
	background-color: var(--menu-spacer-bgcolor) !important;
}
#cms-component-menu a,
#cms-component-menu a:visited {
	color: var(--menu-color);
	background-color: var(--menu-bgcolor);
}
#cms-component-menu a.active,
#cms-component-menu a:hover {
	background-color: var(--menu-active-bgcolor);
}

/*
 * CONTENT
 */
#cms-component-content {
	--content-font-color: #dfd;
	--content-bgcolor: #f7f7ff;
	--content-link-color: #55c;
}
#cms-component-content {
	font-family: 'Rubik', sans-serif;
	color: var(--content-color);
	background-color: var(--content-bgcolor);
	padding: 0.5em;
	min-height: 100%;
}
#cms-component-content a {
	color: var(--content-link-color);
	text-decoration: none;
}

#cms-component-content a,
#cms-component-content a:visited {
	color: var(--content-link-color);
}
#cms-component-content a:hover {
	text-decoration: underline;
}
#cms-component-content table {
	margin: auto;
	text-align: left;
}
#cms-component-content table,
#cms-component-content table td,
#cms-component-content table th {
	border: solid 1px #aaaaaa;
	border-collapse: collapse;
	padding: 0.5em 1em;
}
#cms-component-content table thead,
#cms-component-content table tr.heading,
#output-content table tr.subheading {
	font-weight: bold;
	font-size: 120%;
	color: var(--content-thead-color);
	background-color: var(--content-thead-bgcolor);
}
#cms-component-content table thead {
	position: sticky;
	top: 0px;
	text-align: center;
}
#cms-component-content table tr.subheading {
	text-align: left;
}
#cms-component-content table table {
	font-size: 90%;
}
#cms-component-content table table thead {
	background-color: #ddd;
}
`
	}
	
	// Get the layout
	static async getLayout() {
		return ui.getDemoLayout('grid-3-2'); // just grab one of the UI preconfigs
	}

	// Get the content element ID
	static getContentElementId() {
		return 'ui-content';
	}

	// Get the <head> elements
	static getHeadElements(id) {
		const output = [];
		const pretty_print_json_prefix = "https://cdn.jsdelivr.net/npm/pretty-print-json@2.1/dist/"
		if ( id === 'scripts' ) {
			output.push(pretty_print_json_prefix + "pretty-print-json.min.js");
		}
		else if ( id === 'stylesheets' ) {
			output.push(pretty_print_json_prefix + "css/pretty-print-json.dark-mode.css");
			//output.push(self.getRealURL("UI/style.css"));
		}
		else if ( id === 'fonts' ) {
			output.push("https://fonts.googleapis.com/css2?family=Roboto&family=Rubik");
		}
		return output;
	}

	// Get components
	static async getComponent(id) {
		if ( id === 'header' ) {
			const wrapper = $.ce('div');
		    const h1 = $.ce('h1');
			h1.innerHTML = '<a href="#" style="color:inherit">mod.UI</a>';
			h1.style.fontSize = '36px';
			h1.style.padding = '5px 10px';
			h1.style.fontFamily = 'serif';
		    wrapper.appendChild(h1);
			return wrapper;
		}
		else if ( id === 'footer' ) {
			const wrapper = $.ce('div');
			const p = $.ce('p');
			p.innerHTML = 'cms.js (view on GitLab)';
		    wrapper.append(p);
			return wrapper;
		}
		else if ( id === 'menu' ) {
			const menu = [];
			menu.push({	"action" : "overview", "text" : "Overview" });
			menu.push({ "action" : "layouts", "text" : "Layouts" });
			menu.push({ "action" : "components", "text" : "Components" });
			menu.push({ "action" : "menus", "text" : "Menus" });
			menu.push({ "action" : "stylesheets", "text" : "Stylesheets" });
			menu.push({ "action" : "developers", "text" : "Developers" });
			return ui.buildMenuFromMap(self,menu);
		}
		else if ( id === 'content' ) {
			const wrapper = $.ce('div');
			wrapper.id = 'ui-wrapper';
			wrapper.style.overflow = 'auto';
			wrapper.style.width = '100%';
			wrapper.style.height = '100%';
			wrapper.style.minHeight = '100%';
			const content = $.ce('div');
			content.id = 'ui-content';
//			content.style.minHeight = '100%';
			content.style.padding = "0px 0.5em";
			wrapper.append(content);
			return wrapper;
		}
	}
	
} // end class UI


const allLayouts = new Map();
let map; // placeholder

