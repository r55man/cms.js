
/**
 * Console.
 *
 * This is the base of the UI.
 */
cmsjs.Console = class {

	static Log = cmsjs.Logger.create('cmsjs.Console');

	static Instances = new Map();

	static Initializers = new Map();

	static OriginalDocument = null;

	static CurrentConsole = null;
	
	static setActiveConsole(id) {
		cmsjs.Console.Log.trace('setActiveConsole(id)',id);
		if ( cmsjs.Console.CurrentConsole === id ) {
			cmsjs.Console.Log.debug('setActiveConsole(id) console is already this id:',id);
			return;
		}
		if ( ! cmsjs.Console.Instances.has(id) ) {
			cmsjs.Console.Log.error('setActiveConsole(id) id not found:',id);
			return;
		}
		const console = cmsjs.Console.Instances.get(id);
		if ( ! cmsjs.Console.OriginalDocument ) {
			cmsjs.Console.OriginalDocument = document.documentElement;
		}
		console.attachComponents();
		document.documentElement.replaceWith(console.elements.html);
		cmsjs.Console.CurrentConsole = id;
		// option 2
		//const newDoc = document.implementation.createHTMLDocument();
		//newDoc.documentElement.replaceWith(console.elements.html);
		//document.replaceChild(newDoc.documentElement, document.documentElement);
	}

	/**
	 * Restores the original HTML document that loaded the cmsjs library.
	 */
	static restoreOriginalDocument() {
		cmsjs.Console.Log.trace('restoreOriginalDocument()');
		document.documentElement.replaceWith(cmsjs.Console.OriginalDocument);
		cmsjs.Console.CurrentConsole = null;
	}

	/**
	 * Create a new Console instance.
	 *
	 * Always use this instead of the constructor directly.
	 */
	static create(id,type='default') {
		cmsjs.Console.Log.trace('create(id,type):',id,type);
		if ( cmsjs.Console.Instances.has(id) ) {
			cmsjs.Console.Log.error('constructor() id exists:',id);
		}
		else if ( ! cmsjs.Console.Initializers.has(type) ) {
			cmsjs.Console.Log.error('constructor() no init function found for type:',id,type);
		}
		const instance = new cmsjs.Console(id,type);
		cmsjs.Console.Instances.set(id,instance);
		return instance;
	}

	/**
	 * Constructor.
	 *
	 * Do not call this directly. Use create() instead.
	 */
	constructor(id,initializer) {
		cmsjs.Console.Log.trace('constructor(id,initializer):',id,initializer);
		this.log = cmsjs.Logger.create('cmsjs.Console(' + id + ')');
		this.id = id;
		this.initializer = initializer;
		this.config = { };
		this.elements = { };
		this.initialized = false;
		this.rendered = false;
		this.root = null;
		this.panels = new Map();
		this.initialize();
	}

	/**
	 * Initialize a console instance.
	 *
	 * This is called from the constructor after the initial
	 * configuration. It can also be used to redraw an existing
	 * console.
	 */
	initialize() {
		this.log.trace('initialize()');
		const html = document.createElement('html');
		const head = document.createElement('head');
		const body = document.createElement('body');
		const root = document.createElement('div');
		const overlays = document.createElement('div');
		overlays.style.display = 'none';
		html.appendChild(head);
		html.appendChild(body);
		body.appendChild(root);
		body.appendChild(overlays);
		this.elements.html = html;
		this.elements.head = head;
		this.elements.body = body;
		this.root = root;
		this.overlays = overlays;
		html.setAttribute('cmsjs-html','');
		head.setAttribute('cmsjs-head','');
		body.setAttribute('cmsjs-body','');
		root.setAttribute('cmsjs-console','');
		root.setAttribute('cmsjs-console-id',this.id);
		overlays.setAttribute('cmsjs-overlays','');
		const initializer = cmsjs.Console.Initializers.get(this.initializer);
		initializer(this);
		this.initialized = true;
	}

	/**
	 * Configure grid rows.
	 */
	setGridRows(str) {
		this.log.trace('setGridRows(str)',str);
		this.root.style.gridTemplateRows = str;
	}
	
	/**
	 * Configure grid columns.
	 */
	setGridColumns(str) {
		this.log.trace('setGridColumns(str)',str);
		this.root.style.gridTemplateColumns = str;
	}

	
	setTitle(str) {
		this.log.trace('setTitle(str)',str);
		if ( ! this.elements.title ) {
			const title = document.createElement('title');
			title.setAttribute('cmsjs-title','');
			this.elements.title = title;
			this.elements.head.appendChild(title);
		}
		this.elements.title.innerHTML = str;
		this.config.title = str;
	}

	setFavicon(str) {
		this.log.trace('setFavicon()',str);
		if ( ! this.elements.favicon ) {
			const link = document.createElement('link');
			link.setAttribute('cmsjs-favicon','');
			link.setAttribute('rel','icon');
			this.elements.favicon = link;
			this.elements.head.appendChild(this.elements.favicon);
		}
		this.elements.favicon.setAttribute('href','data:image/x-icon;base64,' + str);
		this.config.favicon = str;
	}

	setFaviconURL(url) {
		this.log.trace('setFaviconURL()',url);
		if ( ! this.elements.favicon ) {
			const link = document.createElement('link');
			link.setAttribute('cmsjs-favicon','');
			link.setAttribute('rel','icon');
			this.elements.favicon = link;
			this.elements.head.appendChild(this.elements.favicon);
		}
		this.elements.favicon.setAttribute('href',url);
		this.config.faviconURL = url;
	}

	addStyles(arg) {
		this.log.trace('addStyles()',arg);
		const args = ( typeof arg === 'string' ) ? [arg] : arg;
		args.forEach((str) => { this.addStyle(str); });
	}
	
	addStyle(str) {
		this.log.trace('addStyle()',str);
		if ( ! this.elements.style ) {
			const style = document.createElement('style');
			style.setAttribute('cmsjs-style','');
			this.elements.style = style;
			this.elements.head.appendChild(this.elements.style);
		}
		this.elements.style.innerHTML += str + '\n';
		this.config.style += str + '\n';
	}
	
	addStylesheets(arg) {
		this.log.trace('addStylesheets()',arg);
		const args = ( typeof arg === 'string' ) ? [arg] : arg;
		args.forEach((str) => { this.addStyleheet(str); });
	}
	
	addStylesheet(str) {
		this.log.trace('addStylesheet()',str);
		const link = document.createElement('link');
		elt.setAttribute('cmsjs-stylesheet','');
		link.setAttribute('rel','stylesheet');
		link.setAttribute('href',str);
		this.elements.head.appendChild(link);
	}

	/**
	 * Adds a <script> tag to the <head> element.
	 */
	addScript(str) {
		this.log.trace('addScripts()',str);
		const script = document.createElement('script');
		script.innerHTML = str;
		this.elements.head.appendChild(script);
	}

	/**
	 * Adds a <script> tag to the <head> element.
	 */
	addScriptLink(url) {
		this.log.trace('addScriptLink()',url);
		const script = document.createElement('script');
		script.setAttribute('cmsjs-script','');
		script.src = url;
		this.elements.head.appendChild(script);
	}
	
	/**
	 * Adds a <script> tag just above the </body> tag.
	 */
	addLazyScript(str) {
		this.log.trace('addLazyScript()',str);
		const script = document.createElement('script');
		script.setAttribute('cmsjs-script','');
		script.innerHTML = str;
		this.elements.body.insertAdjacentElement('beforeend',script);
	}
	
	/**
	 * Adds a <script> tag just above the </body> tag.
	 */
	addLazyScriptLink(url) {
		this.log.trace('addLazyScriptLink()',url);
		script.setAttribute('cmsjs-script','');
		const script = document.createElement('script');
		script.src = url;
		this.elements.body.insertAdjacentElement('beforeend',script);
	}
	
	/**
	 * Adds a panel to the console.
	 *
	 * Note that this does not attach the panel to anything.
	 * For that you must call attachPanel().
	 */
	addPanel(id,config) {
		this.log.trace('addPanel(id)',id);
		const panel = cmsjs.Console.Panel.create(this.id + '.' + id,config);
		panel.element.style.gridRow = panel.row + ' / span ' + panel.rowSpan;
		panel.element.style.gridColumn = panel.col + ' / span ' + panel.colSpan;
		this.root.appendChild(panel.element);
		this.panels.set(id,panel);
		return panel;
	}

	/**
	 * Attaches all components to all panels in this console.
	 */
	attachComponents() {
		this.panels.forEach((panel,id) => {
			panel.attachComponents();
		});
	}


	/**
	 * Removes the specified panel from the DOM.
	 */
	removePanel(id) {
		cmsjs.Console.Log.trace('removePanel()',id);
		const panel = this.panels.get(id);
		panel.remove();
	}
	
	/**
	 * Removes all panels from the DOM.
	 */
	removeAllPanels() {
		cmsjs.Console.Log.trace('removeAllPanels()');
		Array.from(this.panels).forEach((panel) => { panel.remove(); });
	}

}


/**
 * Panel class.
 */
cmsjs.Console.Panel = class {

	static Log = cmsjs.Logger.create('cmsjs.Console.Panel');

	static Instances = new Map();

	static create(id,config) {
		cmsjs.Console.Panel.Log.trace('create(id)',id);
		const newPanel = new cmsjs.Console.Panel(id);
		if ( config ) {
			const parts = config.split(',');
			if ( parts.length === 2 ) {
				[newPanel.row, newPanel.col] = config.split(',');
				[newPanel.rowSpan, newPanel.colSpan] = [1,1];
			}
			else if ( parts.length === 4 ) {
				[newPanel.row,newPanel.col,newPanel.rowSpan,newPanel.colSpan] = config.split(',');
			}
		}
		cmsjs.Console.Panel.Instances.set(id,newPanel);
		return newPanel;
	}

	static get(id) {
		return cmsjs.Console.Panel.Instances.get(id);
	}
	
	constructor(id) {
		cmsjs.Console.Panel.Log.trace('constructor(id)',id);
		this.log = cmsjs.Logger.create('cmsjs.Console.Panel(' + id + ')');
		this.element = document.createElement('div');
		this.element.setAttribute('cmsjs-panel','');
		this.element.setAttribute('cmsjs-panel-id',id);
		this.element.style.overflow = 'hidden';
		this.element.style.boxSizing = 'border-box';
		this.components = [];
	}
	
	setPosition(row,col) {
		this.log.trace('setPosition(row,col)',row,col);
		if ( arguments.length === 1 ) {
			if ( row.includes(',') ) { [row,col] = row.split(','); }
		}
		this.row = row;
		this.col = col;
	}
	setSpan(rows,cols) {
		this.log.trace('setSpan(rows,cols)',rows,cols);
		if ( arguments.length === 1 ) {
			if ( row.includes(',') ) { [row,col] = row.split(','); }
		}
		this.rowSpan = rows;
		this.colSpan = cols;
	}
	getElement() {
		this.log.trace('getElement()');
		return this.element;
	}
	hasComponent(id) {
		this.log.trace('hasComponent(id)',id);
		const component = cmsjs.Component.get(id);
		const componentPanel = component.wrapper.parentElement;
		if ( componentPanel === this.element ) { return true; }
		return false;
	}
	addComponent(id) {
		this.log.trace('attachComponent(id)',id);
		const component = cmsjs.Component.get(id);
		this.components.push(component);
	}
	attachComponents() {
		this.components.forEach((component) => {
			//const component = cmsjs.Component.get(id);
			component.remove();
			component.hide();
			component.build();
			component.show();
			this.element.appendChild(component.getWrapper());
		});
	}
	showOnlyComponent(id) {
		this.log.trace('showOnlyComponent(id)',id);
		if ( ! this.hasComponent(id) ) { return; }
		const component = cmsjs.Component.get(id);
		if ( ! component.isBuilt() ) { component.build(); }
		this.hideAllComponents();
		component.show();
	}
	hideAllComponents() {
		this.log.trace('hideAllComponents()');
		this.components.forEach((component) => {
			component.hide();
		});
	}
	hideComponent(id) {
		this.log.trace('hideComponent(id)',id);
		if ( this.hasComponent(id) ) {
			cmsjs.Component.get(id).hide();
		}
	}
	removeComponent(id) {
		this.log.trace('removeComponent()',id);
		const component = cmsjs.Component.get(id);
		component.remove();
	}
	clear() {
		this.log.trace('clear()');
		this.element.innerHTML = '';
	}
}

cmsjs.Console.Initializers.set('default', (instance) => {
	// do nothing by default
});

cmsjs.Console.Initializers.set('grid-basic', (instance) => {
	const html = instance.elements.html;
	html.style.margin = '0px';
	html.style.padding = '0px';
	html.style.height = '100%';
	html.style.overflow = 'hidden';
	const body = instance.elements.body;
	body.style.margin = '0px';
	body.style.padding = '0px';
	body.style.height = '100%';
	body.style.overflow = 'hidden';
	const root = instance.root;
	root.style.boxSizing = 'border-box';
	root.style.display = 'grid';
	root.style.height = '100%';
	root.style.height = '100%';
	instance.addStyle('* { box-sizing: border-box; }');
	instance.addStyle('a { color: inherit; text-decoration: none; }');
	instance.addStyle('a:hover { text-decoration: underline; }');
	instance.addStyle('::-webkit-color-swatch { border: none; }');
	instance.addStyle('::-webkit-color-swatch-wrapper { padding: 1px 0px; }');
	instance.addStyle('::-moz-color-swatch { border: none; }');
});

cmsjs.Console.Initializers.set('holy-grail', (instance) => {
	cmsjs.Console.Initializers.get('grid-basic')(instance);
	
});


