// This line allows cmsjs.Logger.js to run in standalone mode.
if ( typeof cmsjs === 'undefined' ) { cmsjs = class {}; }

/**
 * Logging.
 *
 * All classes require this class to be defined in order to create 
 * their static class loggers, so it must be the first class loaded
 * after the base class.
 */
cmsjs.Logger = class {

	/**
	 * Grab a handle to the environment console on class load.
	 *
	 * In the event the user carelessly defines a global variable 
	 * called 'console', we want the logger to keep working.
	 */
	static CONSOLE = console;

	/**
	 * Holds the collection of Logger instances.
	 */
	static Instances = new Map();

	/**
	 * Override instance Logger print settings.
	 *
	 * Setting this to 'true' enables all loggers.
	 * Setting this to 'false' disables all loggers.
	 * A value of 'null' disables the override.
	 *
	 * This primarily use of this is for debugging purposes,
	 * providing a one-line shortcut to turn on all logging.
	 */
	static OverrideAll = null;
	
	/**
	 * Override instance Logger print settings.
	 *
	 * As above, except targets specific log levels. The keys
	 * of this Map() are log level identifiers (for example,
	 * 'warn' or 'debug'), and the values are booleans.
	 */
	static OverrideLogLevel = new Map();

	/**
	 * Log if possible. [INTERNAL USE ONLY]
	 *
	 * This class cannot do its own logging until after the class
	 * is defined and default log levels have been created. So for
	 * functions which are called before loggers have been created,
	 * defer to this function instead of calling the log directly.
	 *
	 * @private
	 */
	static _logIfPossible(logLevel, ...args) {
		if ( cmsjs.Logger.Log && cmsjs.Logger.Log[logLevel] ) {
			cmsjs.Logger.Log[logLevel](...args);
		}
	}
	
	/**
	 * Overrides all instance logger print settings for the given
	 * log level.
	 */
	static override(logLevel,value) {
		cmsjs.Logger.Log.trace('override(logLevel,value)',logLevel,value);
		cmsjs.Logger.OverrideLogLevel.set(logLevel,value);
	}
	
	/**
	 * Overrides all instance settings for all log levels.
	 */
	static overrideAll(value) {
		cmsjs.Logger.OverrideAll = value;
	}

	/**
	 * Returns the instance logger with the specified id.
	 */
	static get(id) {
		cmsjs.Logger.Log.trace('get(id)',id);
		return cmsjs.Logger.Instances.get(id);
	}

	/**
	 * Creates a new logger instance.
	 *
	 * Always use this instead of the constructor. The parameter
	 * passed defines the function name of the log level being
	 * created. For example, calling cmsjs.Logger.create('debug')
	 * allows you to later call log.debug("...").
	 *
	 * The value of 'id' must be a valid ES6 function name.
	 *
	 * @param {string} [id] the name of the log level to create
	 */
	static create(id) {
		cmsjs.Logger._logIfPossible('trace','create(id):',id);
		if ( cmsjs.Logger.Instances.has(id) ) {
			cmsjs.Logger._logIfPossible('error','create(id) id exists:',id);
			return null;
		}
		const instance = new cmsjs.Logger(id);
		if ( instance ) { cmsjs.Logger.Instances.set(id,instance); }
		return instance;
	}

	/**
	 * Constructor.
	 *
	 * Never call this directly. Always use create(id) instead.
	 */
	constructor(id) {
		this.ID = id;
		this.Enabled = true;
		this.Flag = null;
		this.Style = null;
		cmsjs.Logger.LogLevel.Instances.forEach((levelObj,levelName) => {
			this._addLogLevel(levelName);
		});
		this.LogLevelEnabled = new Map();
		this.LogLevelFlags = new Map();
		this.LogLevelStyle = new Map();
		this.Time = (new Date()).getTime();
	}

	/**
	 * Enables an individual log level, or the logger as a whole.
	 *
	 * With no arguments, enables the logger as a whole.
	 *
	 * @param {string} [logLevel] the log level to disable
	 */
	enable(logLevel) {
		this.trace('enable(logLevel)',logLevel);
		if ( arguments.length === 0 ) {
			this.Enabled = true;
		}
		else {
			this.LogLevelEnabled.set(logLevel,true);
		}
	}

	/**
	 * Enables all log levels for this logger.
	 *
	 * This is distinct from calling enable(), as it goes through
	 * and enables each defined log level individually.
	 */
	enableAll() {
		this.trace('enableAll()',logLevel);
		this.Enabled = true;
		this.LogLevelEnabled.forEach((value,key) => {
			LogLevelEnabled.set(key,true);
		});
	}
	
	/**
	 * Disables an individual log level, or the logger as a whole.
	 *
	 * With no arguments, disables the logger as a whole.
	 *
	 * @param {string} [logLevel] the log level to disable
	 */
	disable(logLevel) {
		this.trace('disable(logLevel)',logLevel);
		if ( arguments.length === 0 ) {
			this.Enabled = false;
		}
		else {
			this.LogLevelEnabled.set(logLevel,false);
		}
	}
	
	/**
	 * Disables all log levels for this logger.
	 *
	 * This is distinct from calling disable(), as it goes through
	 * and disables each defined log level individually.
	 */
	disableAll() {
		this.trace('disableAll()',logLevel);
		this.Enabled = false;
		this.LogLevelEnabled.forEach((value,key) => {
			LogLevelEnabled.set(key,false);
		});
	}
	
	/**
	 * Sets the flag for the logger or log level.
	 *
	 * A "flag" is simply a string prepended to each log line printed.
	 * It allows different logger instances and different log levels to
	 * have distinct visual cues in a standard location. For all
	 * of the internal loggers, 
	 */
	setFlag(...args) {
		this.trace('setFlag(...args)',...args);
		if ( args.length === 1 ) {
			const flag = args[0];
			this.Flag = flag;
		}
		else {
			const logLevel = args[0];
			const flag = args[1];
			if ( ! cmsjs.Logger.LogLevel.get(logLevel) ) {
				cmsjs.Logger.Log.error('setFlag(logLevel,flagName) log level not found:',logLevel);
				return;
			}
			this.LogLevelFlags.set(logLevel,flag);
		}
	}
	
	/**
	 * Sets the style for the logger.
	 *
	 * A single argument sets the overall style for the logger.
	 *
	 * With two arguments, the first specifies the log level and the 
	 * second is the style 
	 */
	setStyle(...args) {
		this.trace('setStyle(...args)',...args);
		if ( args.length === 1 ) {
			const str = args[0];
			this.Style = str;
		}
		else {
			const logLevel = args[0];
			const str = args[1];
			if ( ! cmsjs.Logger.LogLevel.get(logLevel) ) {
				cmsjs.Logger.Log.error('setStyle(logLevel,str) log level not found:',logLevel);
				return;
			}
			this.LogLevelStyle.set(logLevel,str);
		}
	}


	/**
	 * Adds a log level to this logger.
	 *
	 * @param {string} [logLevel] the name of the log level to add
	 * @private
	 */
	_addLogLevel(logLevel) {
		//if ( this && this.trace ) { this.trace('_addLogLevel(logLevel)',logLevel); }
		if ( this.hasOwnProperty(logLevel) ) {
			cmsjs.Logger.Log.error('_addLogLevel() function exists',logLevel);
			return;
		}
		this[logLevel] = function(str, ...args) {
			if ( this._isEnabled(logLevel) ) {
				this._log(logLevel, str, ...args);
			}
		}
	}

	
	/**
	 * Checks to see if a given log level is enabled.
	 *
	 * @param {string} [logLevel] the name of the log level to check
	 * @private
	 */
	_isEnabled(logLevel) {
		if ( typeof cmsjs.Logger.OverrideAll === 'boolean' ) {
			return cmsjs.Logger.OverrideAll;
		}
		if ( typeof cmsjs.Logger.OverrideLogLevel.get(logLevel) === 'boolean' ) {
			return cmsjs.Logger.OverrideLogLevel.get(logLevel);
		}
		if ( ! this.Enabled ) {
			return false;
		}
		if ( typeof this.LogLevelEnabled.get(logLevel) === 'boolean' ) {
			return this.LogLevelEnabled.get(logLevel);
		}
		return cmsjs.Logger.LogLevel.Instances.get(logLevel).Enabled;
	}
	
	/**
	 * Returns the flag(s) for the specified log level.
	 *
	 * @param {string} [logLevel] name of log level
	 * @private
	 */
	_getFlag(logLevel) {
		let flag = '';
		if ( this.LogLevelFlags.get(logLevel) ) {
			if ( flag.length > 0 ) { flag += ' '; }
			flag += this.LogLevelFlags.get(logLevel);
		}
			if ( flag.length > 0 ) { flag += ' '; }
		if ( this.Flag ) {
			flag += this.Flag;
		}
		if ( cmsjs.Logger.LogLevel.get(logLevel).Flag ) {
			if ( flag.length > 0 ) { flag += ' '; }
			flag += cmsjs.Logger.LogLevel.get(logLevel).Flag;
		}
		if ( flag !== '' ) { return flag; }
		return null;
	}

	/**
	 * Returns the style for the specified log level.
	 *
	 * @param {string} [logLevel] name of log level
	 * @private
	 */
	_getStyle(logLevel) {
		let style = '';
		if ( cmsjs.Logger.LogLevel.get(logLevel).Style ) {
			style += cmsjs.Logger.LogLevel.get(logLevel).Style;
		}
		if ( this.Style ) {
			style += this.Style;
		}
		if ( this.LogLevelStyle.get(logLevel) ) {
			style += this.LogLevelStyle.get(logLevel);
		}
		if ( style !== '' ) { return style; }
		return null;
	}
	
	/**
	 * Log something to the console.
	 *
	 * This should never be called directly. Use the logging functions instead.
	 *
	 * @param {string} [logLevel] name of log level
	 * @param {string} [str] the text to print to the log
	 * @param {...} [...args] additional objects to pass to the logger
	 * @private
	 */
	_log(logLevel, str, ...args) {
		if ( cmsjs.Logger.LogLevel.get(logLevel).Preprocessor ) {
			[str, ...args] = cmsjs.Logger.LogLevel.get(logLevel).Preprocessor(this, str, ...args);
		}
		if ( typeof str === 'string' ) {
			let prefix = '[' + this.ID + '] ' + '<' + logLevel + '>';
			const style = this._getStyle(logLevel);
			if ( style ) {
				prefix = '%c' + prefix;
				args.unshift(style);
			}
			str = prefix + ' ' + str;
			const flag = this._getFlag(logLevel);
			if ( flag ) {
				str = flag + ' ' + str;
			}
		}
		const consoleLevel = cmsjs.Logger.LogLevel.get(logLevel).ConsoleLevel;
		cmsjs.Logger.CONSOLE[consoleLevel](str,...args);
	}

}

/**
 * LogLevel subclass
 *
 * See the default definitions later in this file for usage.
 */
cmsjs.Logger.LogLevel = class {

	/**
	 * Collection of all log levels created.
	 */
	static Instances = new Map();

	/**
	 * Returns the specified log level.
	 */
	static get(id) {
		return cmsjs.Logger.LogLevel.Instances.get(id);
	}

	/**
	 * Returns an array of the names of LogLevel instances that have
	 * been defined.
	 */
	static list() {
		return Array.from(cmsjs.Logger.LogLevel.Instances.keys());
	}
	
	/**
	 * Log if possible.
	 *
	 * This class cannot do its own logging until after it the class
	 * is defined and default log levels have been created. So for
	 * some functions which need to be able to be called before any
	 * loggers have been created, defer to this function instead
	 * of calling the log directly.
	 */
	static _logIfPossible(logLevel, ...args) {
		if ( cmsjs.Logger.LogLevel.Log && cmsjs.Logger.LogLevel.Log[logLevel] ) {
			cmsjs.Logger.LogLevel.Log[logLevel](...args);
		}
	}
	
	/**
	 * Creates a new LogLevel and adds it to the Instances.
	 *
	 * This should always be used instead of the constructor, as it does
	 * sanity checking on the config variable.
	 */
	static create(id,config={}) {
		cmsjs.Logger.LogLevel._logIfPossible('trace','create(id,config)',id,config);
		if ( cmsjs.Logger.LogLevel.Instances.has(id) ) {
			cmsjs.Logger.LogLevel._logIfPossible('error','create(id,config) id exists:',id);
			return null;
		}
		Object.keys(config).forEach((configKey) => {
			const validConfig = ['ConsoleLevel','Enabled','Flag','Style','Processor'];
			if ( ! validConfig.includes(configKey) ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create(id,config) skipping invalid key in config:',configKey);
			}
		});
		// Whether logging is enabled by default
		if ( typeof config['Enabled'] !== 'undefined' ) {
			if ( typeof config['Enabled'] !== 'boolean' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() Enabled should be a boolean, not this:',config['Enabled']);
				config['Enabled'] = true;
			}
		}
		else {
			config['Enabled'] = true;
		}
		// Console logging level
		if ( config['ConsoleLevel'] ) {
			if ( ! cmsjs.Logger.CONSOLE[config['ConsoleLevel']] ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() requested console log command not found:',config['ConsoleLevel']);
				config['ConsoleLevel'] = 'log';
			}
		}
		else {
			config['ConsoleLevel'] = 'log';
		}
		// Flag
		if ( config['Flag'] ) {
			if ( typeof config['Flag'] !== 'string' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() requested flag not a string:',config['Flag']);
				config['Flag'] = null;
			}
		}
		// Style
		if ( config['Style'] ) {
			if ( typeof config['Style'] !== 'string' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() requested style not a string:',config['Style']);
				config['Style'] = null;
			}
		}
		// Preprocessor
		if ( config['Preprocessor'] ) {
			if ( typeof config['Preprocessor'] !== 'function' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() preprocessor is not a function:',config['Preprocessor']);
				config['Preprocessor'] = null;
			}
		}
		const newLogLevel = new cmsjs.Logger.LogLevel(id,config);
		cmsjs.Logger.LogLevel._logIfPossible('info','created new log level with id:',id);
		cmsjs.Logger.LogLevel.Instances.set(id,newLogLevel);
		cmsjs.Logger.Instances.forEach((logger,loggerID) => {
			logger._addLogLevel(id);
		});
		return newLogLevel;
	}

	/**
	 * Constructor. Do not call this directly.
	 *
	 * Always use the static create() method instead, which does
	 * error checking on config.
	 */
	constructor(id,config={}) {
		this.ID = id;
		this.ConsoleLevel = config['ConsoleLevel'] || 'log';
		this.Enabled = (typeof config['Enabled'] === 'boolean') ? config['Enabled'] : null;
		this.Flag = config['Flag'] || null;
		this.Style = config['Style'] || null;
		this.Preprocessor = config['Preprocessor'] || null;
	}
}


/**
 * Flag collection
 *
 * This defines a handful of icons that can be used as flags.
 * Some are mandatory for the cmsjs internal loggers.
 */
cmsjs.Logger.Flags = new Map();
cmsjs.Logger.Flags.set('Ant','\uD83D\uDC1C');
cmsjs.Logger.Flags.set('Fire','\uD83D\uDD25');
cmsjs.Logger.Flags.set('Water','\uD83D\uDCA7');
cmsjs.Logger.Flags.set('Bulb','\uD83D\uDCA1');
cmsjs.Logger.Flags.set('Splash','\uD83D\uDCA6');
cmsjs.Logger.Flags.set('Bell','\uD83D\uDD14');
cmsjs.Logger.Flags.set('Key','\uD83D\uDD11');
cmsjs.Logger.Flags.set('Lock','\uD83D\uDD12');
cmsjs.Logger.Flags.set('Wrench','\uD83D\uDD27');
cmsjs.Logger.Flags.set('Hammer','\uD83D\uDD28');
cmsjs.Logger.Flags.set('Magnet','\uD83E\uDDF2');
cmsjs.Logger.Flags.set('Clock','\uD83D\uDD50');
cmsjs.Logger.Flags.set('Timer','\u23F1');
cmsjs.Logger.Flags.set('Glass','\uD83D\uDD0D');
cmsjs.Logger.Flags.set('Skull','\uD83D\uDC80');
cmsjs.Logger.Flags.set('X','\u274C');
cmsjs.Logger.Flags.set('?','\u2753');
cmsjs.Logger.Flags.set('Pen','\uD83D\uDD8B');
cmsjs.Logger.Flags.set('Happy','\uD83D\uDE00');
cmsjs.Logger.Flags.set('Sad','\uD83D\uDE22');
cmsjs.Logger.Flags.set('Angry','\uD83D\uDE21');
cmsjs.Logger.Flags.set('Cry','\uD83D\uDE2D');
cmsjs.Logger.Flags.set('Crazy','\uD83E\uDD2A');
cmsjs.Logger.Flags.set('Sick','\uD83E\uDD22');
cmsjs.Logger.Flags.set('Cool','\uD83D\uDE0E');


/**
 * Default log levels.
 *
 * Define some common log levels: trace, debug, info, warn, error, fatal
 *
 * Also define a timer that demonstrates Preprocessor usage.
 */
cmsjs.Logger.LogLevel.create('trace', {
	ConsoleLevel: 'log',
	Enabled: false,
	Flag: cmsjs.Logger.Flags.get('Glass'),
	Style: 'color: #aaa;'
});
cmsjs.Logger.LogLevel.create('debug', {
	ConsoleLevel: 'debug',
	Enabled: false,
	Flag: cmsjs.Logger.Flags.get('Ant'),
	Style: 'font-weight: bold;'
});
cmsjs.Logger.LogLevel.create('info', {
	ConsoleLevel: 'info',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('Pen'),
	Style: 'color: #33e;'
});
cmsjs.Logger.LogLevel.create('warn', {
	ConsoleLevel: 'warn',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('Alert'),
	Style: 'color: #930;'
});
cmsjs.Logger.LogLevel.create('error', {
	ConsoleLevel: 'error',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('X'),
	Style: 'color: #a00;'
});
cmsjs.Logger.LogLevel.create('fatal', {
	ConsoleLevel: 'error',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('Skull'),
	Style: 'color: #a00; font-weight: bold;'
});
cmsjs.Logger.LogLevel.create('timer', {
	ConsoleLevel: 'log',
	Enabled: false,
	Flag: cmsjs.Logger.Flags.get('Timer'),
	Style: 'color: #090;',
	Preprocessor: (logger,str,...args) => {
		if ( typeof str !== 'string' ) {
			args = [str, ...args];
			str = '';
		}
		const now = (new Date()).getTime();
		const prev = logger.Time;
		const diff = now - prev;
		str += ' (' + diff + 'ms)';
		logger.Time = now;
		return [str,...args];
	}
});

/**
 * Create loggers for the cmsjs and cmsjs.Logger classes.
 *
 * The cmsjs.Logger class must be defined before loggers can be created.
 * Since the base class (cmsjs) must be defined before cmsjs.Logger it
 * cannot create its own logger. Nor can the cmsjs.Logger class create
 * an instance of itself before it is defined. So these instance
 * loggers are created now.
 */
cmsjs.Log = cmsjs.Logger.create('cmsjs');
cmsjs.Logger.Log = cmsjs.Logger.create('cmsjs.Logger');
cmsjs.Logger.LogLevel.Log = cmsjs.Logger.create('cmsjs.Logger.LogLevel');

