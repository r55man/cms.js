/**
 * State variables.
 *
 * This class is effectively a Map() of key/value pairs with
 * several of auxiliary functions built around it.
 */
cmsjs.State = class {

	/**
	 * Class logger
	 */
	static Log = cmsjs.Logger.create('cmsjs.State');

	/**
	 * Map of state variables.
	 */
	static Map = new Map();

	/**
	 * State change listeners.
	 *
	 * Listeners are triggered when state variables change.
	 * The keys of this Map() should match keys in the 
	 * key/value Map above.
	 */
	static Listeners = new Map();

	/**
	 * Initialize a state variable.
	 *
	 * Call this to set a state variable without saving it and without
	 * triggering an onChange() event.
	 */
	static init(key,value) {
		cmsjs.State.Log.trace('init(key,value)',key,value);
		cmsjs.State.Map.set(key,value);
	}

	/**
	 * Get the value of a state variable.
	 */
	static get(key,arg) {
		return cmsjs.State.Map.get(key,arg)
	}

	/**
	 * Add a state change listener.
	 *
	 * By default, state change listeners fire whenever the value of
	 * state variable changes.
	 */
	static addListener(key, onChange) {
		cmsjs.State.Log.trace('addListener(key)',key);
		if ( ! cmsjs.State.Map.has(key) ) {
			cmsjs.State.Log.error('addListener(key,onChange) key not found:',key);
			return;
		}
		cmsjs.State.Listeners.set(key,onChange);
	}
	
	/**
	 * Add a state change listener.
	 *
	 * Call this to set a value without saving it.
	 *
	 * The third parameter is a boolean that controls whether or not to
	 * fire the listener. 'true' always fires, 'false' always does not,
	 * and if omitted it only fires if the new value is different.
	 */
	static set(key,value,fireOnChange=null) {
		cmsjs.State.Log.trace('set(key,value)',key,value);
		if ( ! cmsjs.State.Map.has(key) ) {
			cmsjs.State.Map.set(key,value);
		}
		else {
			const currentValue = cmsjs.State.Map.get(key);
			cmsjs.State.Map.set(key,value);
			if ( currentValue && cmsjs.State.Listeners.has(key) ) {
				if ( fireOnChange === true || ( fireOnChange !== false && currentValue !== value ) ) {
					cmsjs.State.Listeners.get(key)(value);
				}
			}
		}
	}

	/**
	 * Save a state variable.
	 *
	 * Saves a state variable to localStorage and/or browser-specific
	 * storage.
	 */
	static save(key,value) {
		cmsjs.State.Log.trace('save(key,value)',key,value);
		const jsonString = JSON.stringify(value);
		localStorage.setItem('cmsjs.State:' + key, jsonString);
		if ( typeof chrome !== 'undefined' && chrome && chrome.storage && chrome.storage.local ) {
			const chromeKey = key.replace('.','_');
			chrome.storage.local.set({ [chromeKey]: jsonString }, () => {
				//cmsjs.Log.debug('chrome settings saved chromeKey = ' + chromeKey + ' and jsonString = ',jsonString);
			});
		}
	}

	/**
	 * Set and save a state variable.
	 */
	static setAndSave(key,value) {
		cmsjs.State.Log.trace('setAndSave(key,value)',key,value);
		cmsjs.State.set(key,value);
		cmsjs.State.save(key,value);
	}

	/**
	 * Toggle a boolean state variable.
	 *
	 * This triggers the onChange() listener.
	 */
	static toggle(key) {
		cmsjs.State.Log.trace('toggle(key)',key);
		const value = cmsjs.State.get(key);
		if ( typeof value !== 'boolean' ) {
			cmsjs.Log.error('{cmsjs.State} not a boolean value: ',value);
			return undefined;
		}
		if ( value === true ) {
			cmsjs.State.change(key,false);
			return false;
		}
		else {
			cmsjs.State.change(key,true);
			return true;
		}
	}

	/**
	 * Toggle and save a boolean state variable.
	 *
	 * This triggers the onChange() listener.
	 */
	static toggleAndSave(key) {
		cmsjs.State.Log.trace('toggleAndSave(key)',key);
		const value = cmsjs.State.toggle(key);
		cmsjs.State.save(key,value);
	}

	/**
	 * Change a state variable.
	 *
	 * This behaves like set(), but triggers the onChange() listener
	 * regardless of whether or not the new value differs.
	 */
	static change(key,value) {
		cmsjs.State.Log.trace('change()',key,value);
		cmsjs.State.set(key,value,true);
	}

	/**
	 * Change and save state variable.
	 *
	 * This behaves like setAndSave(), but triggers the onChange() listener
	 * regardless of whether or not the new value differs.
	 */
	static changeAndSave(key,value) {
		cmsjs.State.Log.trace('changeAndSave(key,value)',key,value);
		cmsjs.State.change(key,value,true);
		cmsjs.State.save(key,value);
	}

	/**
	 * Load all defined state variables from localStorage.
	 */
	static loadLocalStorage() {
		cmsjs.State.Log.trace('loadLocalStorage()');
		Array.from(cmsjs.State.Map.keys()).forEach((key) => {
			const val = localStorage.getItem('cmsjs.State:' + key);
			let parsedVal = null;
			if ( val !== undefined && val !== null ) {
				try {
					parsedVal = JSON.parse(val);
				}
				catch (error) {
					cmsjs.State.Log.error('loadLocalStorage() invalid JSON for key "' + key + ' ": ',val);
					cmsjs.State.Log.error(error);
				}
				cmsjs.State.Map.set(key,parsedVal);
				cmsjs.State.Log.debug('loadLocalStorage() set key/value:',key,parsedVal);
			}
		});
	}

	/**
	 * Load all defined state variables from chrome.storage.
	 */
	static async loadChromeStorage() {
		cmsjs.State.Log.trace('loadChromeStorage()');
		/*
		const chromeKeys = [];
		const keymap = {};
		Array.from(cmsjs.State.Map.keys()).forEach((key) => {
			const chromeKey = key.replace(/\./g,'_');
			chromeKeys.push(chromeKey);
			keymap[chromeKey] = key;
		});
		*/
		const allKeys = Array.from(cmsjs.State.Map.keys());
		const result = await new Promise((resolve) => {
			chrome.storage.local.get(allKeys, (data) => {
				resolve(data);
			});
		});
				
		allKeys.forEach((key) => {
			// result is an array of all of the values of allKeys
			if ( key in result ) {
				const val = result[key];
				if ( val !== undefined ) {
					let parsedVal = null;
					try {
						parsedVal = JSON.parse(val);
					}
					catch (error) {
						cmsjs.State.Log.error('loadChromeStorage() invalid JSON for key "' + key + '": ',val);
						cmsjs.State.Log.error(error);
					}
					if ( key.includes('_') ) {
						const namespace = key.split('_')[0];
						const realKey = key.replace(namespace + '_','');
						const map = cmsjs.State.NamespaceMaps.get(namespace);
						map.set(realKey,parsedVal);
					}
					else {
						const map = cmsjs.State.DefaultMap;
						map.set(key,parsedVal);
					}
					cmsjs.State.Log.debug('loadChromeStorage() set ' + (key.replace('_','.')) + ': ',parsedVal);
				}
			}
		});
	}

	/**
	 * Workaround for the insanity of Chrome's async local storage access.
	 */
	static _dealWithChromeBS() {
	}

	/**
	 * Set a cookie.
	 *
	 * 'name' and 'value' are mandatory; 'config' is optional
	 *
	 * Valid options for config are (not case-sensitive):
	 *
	 *   Domain		Defaults to current domain
	 *   Path		Defaults to current path
	 *   Expires	Expiration date (default none, expires at end of session)
	 *   Max-Age	Maximum age of cookie (defaut none, expires at end of session)
	 *		NOTE: If both Expires and Max-Age are specified, Expires will be ignored and only Max-Age will be used
	 *   Secure		Only send cookie over HTTPS (default false)
	 *   HttpOnly	Do not allow scripts to manipulate the cookie (default false)
	 *   SameSite	'Strict', 'Lax', or 'None' (default varies by browser)
	 *
	 * setCookie(name,value,{'Domain':'.example.com','Max-Age':30d,'Secure':true}
	 */
	setCookie(name, value, config={}) {
		cmsjs.State.Log.trace('setCookie(name,value)',name,value);
		let cookieString = name + '=' + encodeURIComponent(value);
		Object.entries(config).forEach(([key,value]) => {
			if ( key.toLowerCase() === 'expires' ) {
				const daysToExpire = value;
				const msToExpire = daysToExpire * 24 * 60 * 60 * 1000;
				const expirationDate = new Date(Date.now() + msToExpire).toUTCString();
				cookieString += '; Expires=' + expirationDate;
			}
			else if ( key.toLowerCase() === 'max-age' ) {
				const daysToExpire = value;
				const secsToExpire = daysToExpire * 24 * 60 * 60;
				cookieString += '; Max-Age=' + secsToExpire;
			}
			else if ( key.toLowerCase() === 'domain' ) {
				cookieString += '; Domain=' + value;
			}
			else if ( key.toLowerCase() === 'path' ) {
				cookieString += '; Path=' + value;
			}
			else if ( key.toLowerCase() === 'secure' && value === true ) {
				cookieString += '; Secure';
			}
			else if ( key.toLowerCase() === 'httponly' && value === true ) {
				cookieString += '; HttpOnly';
			}
			else if ( key.toLowerCase() === 'samesite' ) {
				cookieString += '; SameSite=' + value;
			}
		});
		cmsjs.State.Log.debug('setting cookie: ',cookieString);
		document.cookie = cookieString;
	}

	/**
	 * Get a cookie value by name.
	 */
	getCookie(name) {
		cmsjs.State.Log.trace('getCookie(name)',name);
		const cookies = document.cookie.split(';');
		for ( const cookie of cookies ) {
			const [cookieName, cookieValue] = cookie.trim().split('=');
			if ( cookieName === name ) {
				return decodeURIComponent(cookieValue);
			}
		}
		return null;
	}

}

