
/**
 * Modules
 */
cmsjs.Module = class {

	static Log = cmsjs.Logger.create('cmsjs.Module');

	static Instances = new Map();

	static Config = {
		ModuleRoot: 'mod.root',
		ModuleDirectoryPrefix: 'mod.'
	};
	
	static create(id) {
		if ( cmsjs.Module.Instances.has(id) ) {
			cmsjs.Module.Log.error('create(id) id exists:',id);
			return;
		}
		const instance = new cmsjs.Module(id);
		cmsjs.Module.Instances.set(id,instance);
		return instance;
	}

	constructor(id) {
		cmsjs.Module.Log.trace('constructor()',id);
		this.log = cmsjs.Logger.create('cmsjs.Module(' + id + ')');
		this.id = id;
		this.path = id;
		this.config = {};
		this.script = null;
	}

	// Load module (return from cache if already loaded)
	static async load(path) {
		cmsjs.Module.Log.track('load(path)',path);
		if ( ! path || ! path.startsWith('/') || ! path.endsWith('/') ) {
			cmsjs.Module.Log.error('invalid path:',path);
			return null;
		}
		if ( ! Module.Loaded.has(path) ) {
			cmsjs.Module.Log.info('load(' + path + ') [cache miss]');
			const instance = new Module(path);
			let loadScript = null;
			if ( path === '/' ) {
				loadScript = cmsjs.Module.Config.ModuleRoot.replace(new RegExp('^' + cmsjs.Module.Config.ModuleDirectoryPrefix),'') + '.js';
			}
			else {
				const parts = path.split('/').filter(part => part.trim() !== '');
				loadScript = parts[parts.length - 1] + '.js';
			}
			const url = instance.getRealURL(loadScript);
			console.warn(url);
			const script = await import(url);
			instance.script = script;
			if ( script.load.toString().includes('async') ) {
				await script.load(Module,instance);
			}
			else {
				script.load(Module,instance);
			}
			if ( ! instance.isStandalone() && path != '/' ) {
				const tmp = path.slice(0,-1);
				const parent = tmp.substring(0,tmp.lastIndexOf('/')+1);
				await Module.load(parent);
			}
			const keys = Object.keys(script);
			keys.forEach(key => { instance[key] = script[key]; });
			Module.Loaded.set(path,instance);
		}
		const mod = Module.Loaded.get(path);
		return mod;
	}

	// Returns a module from the cache. Useful when you know a
	// module will be loaded and don't want to use async.
	static get(path) {
		return Module.Loaded.get(path);
	}

	// All modules are considered standalone unless they explicitly
	// have config['StandAlone'] = false;
	isStandalone() {
		if ( this.path === '/' ) { return true; }
		if ( ! 'StandAlone' in this.config ) { return true; }
		if ( this.config['StandAlone'] !== false ) { return true; }
		return false;
	}
	
	// Returns the parent module if this module is not standalone
	getParent() {
		if ( this.isStandalone() ) { return null; }
		const chopped = this.path.slice(0,-1);
		const parentPath = chopped.substring(0,chopped.lastIndexOf('/')+1);
		return Module.get(parentPath);
	}
	
	// Gets an array of parent modules, including this one, from root down.
	getLineage() {
		let lineage = [];
		lineage.push(this);
		let module = this;
		while ((module = module.getParent()) != null) {
			lineage.push(module);
		}
		lineage.reverse();
		return lineage;
	}

	// Gets an array of all parents (only parents, not this module)
	getParents() {
		return this.getLineage().pop();
	}

	// Gets the root module of a package
	getPackageRoot() {
		return this.getLineage()[0];
	}

	// Gets a module relative to the package root
	getPackageModule(module) {
		return Module.get(this.getPackageRoot().path.slice(0,-1) + module);
	}
	
	// Gets rid of garbage in the path, such as /./ or //
	// Also resolves /../ up to the virtual document root.
	static resolvePath(path) {
		cmsjs.Module.Log.trace('resolvePath(path)',path);
		const parts = path.split('/');
		const stack = [];
		for ( const part of parts ) {
			if ( part === '..' ) { stack.pop(); }
			else if ( part !== '.' && part !== '' ) { stack.push(part); }
		}
		// The stack loses the leading and trailing slash. Also we must
		// account for when the stack is empty and not return two slashes.
		let resolvedPath = '/' + stack.join('/');
		if ( resolvedPath != '/' ) {
			resolvedPath += '/';
		}
		return resolvedPath;
	}

	// Gets the real URL to a resource owned by this module.
	// This is generally too verbose to log
	getRealURL(resource) {
		//this.log.trace('getRealURL(resource)',resource);
		if ( ! resource ) { resource = ''; }
		const realRoot = ENV.RealURL.pathname + cmsjs.Module.Config.ModuleRoot;
		const prefix = '/' + cmsjs.Module.Config.ModuleDirectoryPrefix;
		const realPath = this.path.slice(0,-1).replace(/\//g, prefix) + '/';
		const realURL = realRoot + realPath + resource;
		return realURL;
	}

	// Gets a link for use within a tag like <a href="...">
	// This is generally too verbose to log
	getVirtualURL(resource) {
		//this.log.trace('getVirtualURL(resource)',resource);
		if ( ! resource ) { resource = ''; }
		if ( ! resource.startsWith('/') ) {
			resource = this.path + resource;
		}
		const virtualURL = '#' + resource;
		return virtualURL;
	}

	// Fetches a file relative to the module's directory
	async fetchFile(url) {
		this.log.trace('fetchFile(url)',url);
		//const config = await fetch(url).then(r => (r.ok ? r.json() : {})).catch(() => ({}));
		// should get error checking in here
		const logstr = 'fetchFile(' + url + ')';
		const realURL = this.getRealURL(url);
		const filename = realURL.slice(realURL.lastIndexOf('/') + 1);
		const response = await fetch(realURL,{cache:'no-store'});
		const ct = response.headers.get('content-type');
		if ( ! ct || typeof ct !== 'string' ) {
			this.log.error('invalid Content-Type in response',ct);
			ct = 'application/octet-stream';
		}
		let output = null;
		const text = await response.text();
		// Dynamically loaded function file
		if ( filename.startsWith('f.') && filename.endsWith('.js') ) {
			this.log.info(filename);
			const script = new Function(text);
			output = script();
		}
		// JSON data
		else if ( filename.endsWith('.json') ) {
			output = JSON.parse(text);
		}
		// Any other text
		else if ( ct.startsWith('text/') ) {
			output = text;
		}
		// Binary data
		else {
			output = await response.blob();
		}
		return output;
	} // ends fetchFile(url)

	writePackageMetaTag() {
		const pkgRoot = this.getPackageRoot();
		const metaTag = document.head.querySelector('meta[cms-package]');
		if ( metaTag ) {
			metaTag.setAttribute('cms-package',pkgRoot.path)
		}
		else {
			const newMetaTag = document.createElement('meta');
			newMetaTag.setAttribute('cms-package',pkgRoot.path);
			document.head.appendChild(newMetaTag);
		}
	}


	
	/*
	 * Runs the specified action from the module script.
	 * Specified params are added to the params from the URL
	 * search. They may be sent as either maps or objects.
	 * Params sent in the query string override those called directly.
	 */
	async action(id, params={}) {
		this.log.trace('action(id,params={})',{id:id,params:params});
		// Convert passed params to a Map
		const paramMap = params instanceof Map ? params : new Map(Object.entries(params));
		// Append Map from URL params
		await this.script.init();
		ENV.Params.forEach((value, key) => {
			// NOTE: If you take away this conditional, ENV overwrites passed params.
			if ( ! paramMap.has(key) ) {
				paramMap.set(key, value);
			}
		});
		if ( ! id ) { id = '.'; }
		const logstr = '<' + id + '>';
		const actions = this.script.actions;
		this.log.trace(logstr,params);
		let output = null;
		if ( actions.has(id) ) {
			this.writePackageMetaTag();
			await actions.get(id)(paramMap);
		}
		else if ( actions.has('*') ) {
			this.log.error(logstr + ' action not found, running default');
			this.writePackageMetaTag();
			await actions.get('*')(paramMap);
		}
		else  {
			this.log.error(logstr + ' invalid action, valid actions:',actions);
		}
	}

	// Redirect to a new virtual url
	redirect(url) {
		this.log.trace('redirect(url)',url);
		url = this.getVirtualURL(url);
		history.replaceState(null,null,url);
		window.location.hash = url;
		window.dispatchEvent(new Event('hashchange'));
	}

	// Provides modules access to environment variables.
	env(variable) {
		return ENV[variable];
	}

	// Set a cookie
	setCookie(name, value) {
		this.log.trace('setCookie()',name);
		this.log.warn('setCookie(): realRoot is hard coded, need to pull from ENV');
		const config = {
			'max-age' : 30,
			'path' : '/'
		};
		cmsjs.State.setCookie(name,value,config);
	}

	// Get a cookie
	getCookie(name) {
		this.log.trace('getCookie(name)',name);
		return cmsjs.State.getCookie(this.path + name);
	}
	
}

