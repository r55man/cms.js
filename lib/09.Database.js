
cmsjs.Database = class {

	static Log = cmsjs.Logger.create('cmsjs.Database');

	static Instances = new Map();

	static create(id) {
		if ( cmsjs.Database.Instances.has(id) ) {
			cmsjs.Database.Log.error('create(id) id exists:',id);
			return;
		}
		const instance = new cmsjs.Database(id);
		cmsjs.Database.Instances.set(id,instance);
		return instance;
	}

	constructor(id) {
	}

	addDataSource() {
	}

	addDataURL(id,url) {
	}

	addData(id,data) {
	}

	addRemoteData(id,url) {
	}

	getData(id) {
	}

	getRemoteData(id) {
	}
	
	static processStringKeys(data,lang,prefix) {
	    if (Array.isArray(data)) {
		    data.forEach(item => processStringKeys(item,lang,prefix));
	    }
		else if (typeof data === 'object' && data !== null) {
			Object.entries(data).forEach(([k,v]) => {
				if (typeof v === 'string' && v.startsWith(prefix) && lang[v.slice(2)] !== null ) {
	                data[k] = lang[v.slice(2)];
		        } else {
			        processStringKeys(v,lang,prefix);
				}
	        });
	    }
	}
	
}

/**

Taken from the old mod.DB file
	
export async function getData(module,id,lang='en') {
    self.log.track('<get-data>',arguments);
    if ( ! module ) {
        self.error('missing parameter: module');
    }
    const dataRoot = module.getRealURL() + 'DB'
    const dataDir = dataRoot + '/' + id;
    const dataURL = dataDir + '/' + 'data.json';
    const langURL = dataDir + '/' + 'data.lang.' + lang + '.json';
    let t0 = self.log.timer();
    let data = await fetch(dataURL).then((r) => r.json());
    //t0 = self.log.timer('[loaded data]',t0);
    let langStrings = await fetch(langURL).then((r) => r.json());
    //t0 = self.log.timer('[loaded strings]',t0);
    processStringKeys(data,langStrings,'$.');
    //self.log.timer('[applied strings]',t0);
    self.log.trackout('<get-data>');
    return data;
}

*/
